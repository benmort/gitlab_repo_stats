"""."""
import setuptools


setuptools.setup(
    name="ska-gitlab-branch-stats",
    version="0.0.1",
    description="Utility for generating gitlab branch stats for SKA GitLab",
    long_description='',
    license='BSD-3-Clause',
    author='Ben Mort',
    author_email='',
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    classifisers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Topic :: Utilities'
    ],
    python_requires=">3.7",
    install_requires=[
    ]
)