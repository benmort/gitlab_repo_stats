"""Logging module."""
import sys
import logging
import os
from copy import copy

LOGGER_NAME = "repo-stats"
LOG = logging.getLogger(LOGGER_NAME)


class ColouredFormatter(logging.Formatter):
    """."""

    mapping = {
        "DEBUG": 36,  # white
        "INFO": 32,  # cyan
        "WARNING": 33,  # yellow
        "ERROR": 31,  # red
        "CRITICAL": 41,  # white on red bg
    }

    prefix = "\033["
    suffix = "\033[0m"

    def __init__(self, fmt, datefmt=None):
        """."""
        logging.Formatter.__init__(self, fmt, datefmt)

    def format(self, record):
        """."""
        _record = copy(record)
        levelname = _record.levelname
        seq = self.mapping.get(levelname, 37)  # default white
        _levelname = f"{self.prefix}{seq}m{levelname}{self.suffix}"
        _record.levelname = f"{_levelname: <18}"
        return logging.Formatter.format(self, _record)


def init_logger(level="INFO"):
    """Initialise the logger."""
    logger = logging.getLogger(LOGGER_NAME)
    handler = logging.StreamHandler(sys.stdout)
    format_str = "%(asctime)s %(levelname)s %(message)-120s [%(filename)s:L%(lineno)d]"
    formatter = ColouredFormatter(format_str, "%H:%M:%S")

    handler.setFormatter(formatter)
    logger.addHandler(handler)
    if bool(int(os.getenv("ZOOM_KEYHOLE_FILE_LOGGER", "0"))):
        print("Logging to file!")
        file_handler = logging.handlers.RotatingFileHandler(
            "zkh.log", maxBytes=(1024 * 1024), backupCount=30
        )
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    logger.setLevel(level)
