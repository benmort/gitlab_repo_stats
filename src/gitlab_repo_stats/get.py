# coding: utf-8
"""."""
from datetime import datetime, timezone

import gitlab
import numpy as np

from .logging import LOG


def get_stats(project_name_with_namespace):
    """."""
    LOG.debug("Connecting to GitLab ...")
    gl = gitlab.Gitlab.from_config("ska", ["./gl.cfg"])

    stats = dict()

    # Name and URL
    project = gl.projects.get(project_name_with_namespace)
    # LOG.debug("** %s", project)
    LOG.debug("Name:        %s", project.name)
    LOG.debug("Visibility:  %s", project.visibility)
    LOG.debug("URL:         %s", project.web_url)
    LOG.debug("Created:     %s", project.created_at)
    LOG.debug("Last update: %s", project.last_activity_at)
    LOG.debug("Namespace:   %s", project.namespace["web_url"])
    LOG.debug("Path:        %s", project.path_with_namespace)
    LOG.debug("Tags:        %s", project.tag_list)
    LOG.debug("Description: %s", project.description)

    stats["name"] = project.name
    stats["url"] = project.web_url
    stats["visibility"] = project.visibility
    stats["last_activity_at"] = project.last_activity_at

    # Badges
    stats["badges"] = {}
    LOG.debug("Badges:")
    for badge in project.badges.list():
        badge_links = project.badges.render(badge.link_url, badge.image_url)
        LOG.debug(" - Name:  %s", badge.name)
        LOG.debug(" - Link:  %s", badge_links["rendered_link_url"])
        LOG.debug(" - Image: %s", badge_links["rendered_image_url"])
        stats["badges"][badge.name] = dict(
            url=badge_links["rendered_link_url"],
            image=badge_links["rendered_image_url"],
        )

    # Branches
    branches = project.branches.list(all=True)
    now = datetime.now(timezone.utc)
    fmt = "%Y-%m-%dT%H:%M:%S.%f%z"
    branch_ages = [
        (now - datetime.strptime(branch.commit.get("committed_date"), fmt)).days
        for branch in branches
    ]
    branch_ages = np.array(branch_ages)

    LOG.debug("Branches:")
    LOG.debug("  Number:   %d", len(branch_ages))
    LOG.debug("  Min age:  %f", float(np.min(branch_ages)))
    LOG.debug("  Max age:  %f", float(np.max(branch_ages)))
    LOG.debug("  Mean age: %f", float(np.mean(branch_ages)))

    return stats
